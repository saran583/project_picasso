package com.sample.picasso;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class Activity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);

        ImageView image1=(ImageView)findViewById(R.id.im);
        Picasso.with(getApplicationContext()).load("http://96.0.6.144/images/logo.jpg").into(image1);

        ImageView image2=(ImageView)findViewById(R.id.ic);
        Picasso.with(getApplicationContext()).load("http://www.androidguys.com/wp-content/uploads/2014/07/android-for-wallpaper-8.png").into(image2);
    }
}
